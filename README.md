# L2J Docker Geodata 

Geodata files for L2JServer

**(This repository is going to be merged with [l2jserver/l2j-server-docker](https://bitbucket.org/l2jserver/l2j-server-docker/) )**

## Contributors

- HorridoJoho
- NosBit
- maneco2

## Support links

https://www.l2jserver.com/forum/viewtopic.php?f=89&t=31429

# License

L2J Server is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

Please read the complete [LICENSE](https://bitbucket.org/nonom/docker-l2j-server/src/master/LICENSE.md)