FROM alpine:latest

RUN mkdir -p /opt/l2j/server/geodata

COPY target/GeoData_Rev2.zip  /opt/l2j/server/geodata/.  

RUN unzip /opt/l2j/server/geodata/*.zip && \
    rm -f /opt/l2j/server/geodata/*.zip

VOLUME ["/opt/l2j/server/geodata"]

CMD tail -f /dev/null 